Основы git.
1. Скачайте гит для своей версии ОС и установите его 
https://git-scm.com/download/win

2. Запустите git cmd и настройте параметры пользователя командами: 
git config --global user.name "Ваше_Имя"
git config --global user.email Ваша_Электронная_Почта

3. Выберете проект, с которым будем работать. 

4. Откройте GIT Bash и перейдите в каталог, где находится ваш проект: 
cd C:/Users/Админ/Desktop/test\ project/

5. Затем, для инициализации гит в папке проекта, выполните:
git init 
git add .

6. Для того, чтобы сделать свой первый коммит выполните:
git commit -am “Ваш комментарий”
Например: git commit -am "Init"

7. Зарегистрируйтесь на сайте https://gitlab.com/ и создайте удалённый репозиторий (желающие могут использовать github)
Для создания репозитория нужно кликнуть на зелёную кнопку «new project»
Затем вписать название проекта и выбрать настройку «публичныый»

8. Для подключения будем использовать ssh. Для того, чтобы его сгенерировать, запустите консоль git и выполните команду (в кавычках нужно указать свой почтовый ящик): 
ssh-keygen -t rsa -b 2048 -C "email@example.com"

После выполнения команды, несколько раз нажимаем enter, или ввести ключевое слово для шифрования. 
После успешного выполнения, будет создана пара ключей в папке пользователя ОС (C:/Users/ваш_пользователь/.ssh)
В ней будет 2 текстовых файла ключей, публичный и приватный. Публичный добавляем на сайт в поле «ключ» (github, gitlab). 
Приватный не трогаем и никому не показываем. На сайте можно настроить подпись ключа и срок его действия.

9. Запустите git bash:
git remote add <shortname> <url> 
git push <shortname> master
Где <url> - ссылка на удалённый репозиторий, а <shortname> имя проекта, которого делаем коммит.
После выполнения, система запросит ввести в форму данные своей учётной записи. 
Пример:
git remote add KGU_Web_Project_django https://gitlab.com/denis.karandashev/kgu_web_project_django
git push KGU_Web_Project_django master


10. Посмотрите изменения на сайте в созданном репозитории.

11. Для того, чтобы скопировать себе проект из удалённого репозитория используется команда clone, например: 
git clone https://github.com/schacon/ticgit


Мои действия:
ВНИМАНИЕ! Указаны команды для Mac OS, для Windows вместо python3 надо писать python 
1. Создать папку проекта, назвать django_site
2. cd /Users/sergey/Documents/Study/КГУ/django_site
3. django-admin startproject mysite
Проверяем, создалась ли подпапка mysite с вложенными файлами.
4. cd /Users/sergey/Documents/Study/КГУ/django_site/mysite
5. python3 manage.py runserver  
6. python3 manage.py migrate (если потребуется)
7. python3 manage.py startapp blog 
Проверяем, создалась ли подпапка blog с вложенными файлами.

8. В папке blog создаем файл urls.py:
from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.index, name='index'),
]

9. В папке blog в файл views.py вносим изменения:
from django.shortcuts import render
from django.http import HttpResponse

def index(request) :
    return HttpResponse("Это работает!")

10. В папке mysite в файл urls.py вносим изменения:
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('blog/', include('blog.urls')),
]

11. Проверяем: 
0) cd /Users/sergey/Documents/Study/КГУ/KGU_Web_Project_django/mysite
1) python3 manage.py runserver  
2) http://127.0.0.1:8000/ (увидеть ракету)
3) http://127.0.0.1:8000/blog (увидеть текст из п. 9)

12. Создадим отдельную директорию apps для будущих приложений (таких, как уже созданное blog) - не рекомендуемая практика, но удобнее:
1) создадим новую директорию apps:
mysite/mysite/apps
2) перенесем туда уже созданные приложения (у нас это blog)
3) внесем изменения в mysite/settings.py, 

--== добавив после from pathlib import Path: ==--import os, sys
PROJECT_ROOT = os.path.dirname(__file__)
sys.path.insert(0, os.path.join(PROJECT_ROOT, 'apps'))

--== и чуть ниже - дописать 'blog' ==--
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'blog',
]

4) проверим (см. п. 11)
5) если все ок, заливаем на гит:
5.1) меняем директорию на корневую
cd /Users/sergey/Documents/Study/КГУ/KGU_Web_Project_django
5.2) создаем локальный кормит
git commit -am "Add apps directory"
5.3) пушим на удаленный репозиторий в ветку master
git push KGU_Web_Project_django master
6) еще раз запускаем (п. 11) 

=============================================================
              ---=== Занятие 05.12.2020 ===---
1. создали  second:
1) --== mysite/apps/blog/urls.py ==--
urlpatterns = [
    path('', views.index, name='index'),
    path('second', views.second, name='second'),
]
2) --== mysite/apps/blog/views.py ==--
def second(request) :
    return HttpResponse("Это тоже работает!")

3) теперь запускаем сервер и проверяем по адресу 
http://127.0.0.1:8000/blog/second

2. models.py, описываем бд: 
class Article(models.Model):
    title = models.CharField('Article title', max_length=120)
    body = models.TextField('Text')
    date = models.DateTimeField('Date of publication')
class Comment(models.Model):
    # для связи с конкретной статьёй
    # если статья удаляется, то удаляются и комментарии к ней
    article = models.ForeignKey(Article, on_delete=models.CASCADE) 
    author = models.CharField('Author,s name', max_length=30)
    comment_text = models.CharField('Text of comment', max_length=220)

3. миграция
python3 manage.py makemigrations
python3 manage.py migrate  

4. создание суперюзера
python3 manage.py createsuperuser
Задаем логин и пароль (пароль не будет отображаться при наборе)
http://127.0.0.1:8000/admin/ - заходим и вводим заданные логин и пароль

5. добавляем в admin.py
from blog.models import Article, Comment
admin.site.register(Article)
admin.site.register(Comment)

6. проверяем 
http://127.0.0.1:8000/admin/


=============================================================
              ---=== Занятие 08.12.2020 ===---

изменения в аппсторе
перезагрузка сервера

меняем дизайн админки https://grappelliproject.com/
pip3 install django-grappelli



https://django-bootstrap4.readthedocs.io/en/latest/installation.html


css beautifier

python3 manage.py collectstatic


==== чужой конспект:

повторение

1= создание 2х классов
2= создание суперпользователя.

3= vsc - apps.py -intercode := 0000 нафиг не надо

4= vcs - (куда??? cllr\ccs models.py)  def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'

5= https://grappelliproject.com/    install with instruction

6= install apps vsc add url for admin.py 

   STATIC_DIR = os.path.join(BASE_DIR, 'static')


7= create foulder папка какая то? statik

8= установкаs css js. rar https://getbootstrap.com/

9 = файл копируем в папку templates  + blog

10 = wrapper.html и homepage.html

11 = wrapper - шаблон вставка <!DOCTYPE html>
<html lang="en">
<head>
    {% load static %}
    <link rel="stylesheet" href="{% static 'css/bootstrap.min.css'%}" type="text/css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Заголовок страницы</title>

</head>
<body style="background: silver">
    {% block content %}
    {% endblock %}    
</body>
</html>

12 = homepage {% extends "blog/wrapper.html" %}
{% block content %}
<h1>Проверка!</h1>
<div class="jumbotron">
    <div class="container">
    <p>
    <a href="/" class="btn btn-success btn-lg">Главная страница</a>
    <a href="/news" class="btn btn-primary btn-lg">Узнать больше &raquo; </a>
    </p>
    </div>
    </div>
    <div class="container">
    
    <hr>
    &copy; Все права защищены 2018
    </div>
{% endblock %}
13 = settings.py    STATIC_URL = '/static/'
STATIC_DIR = os.path.join(BASE_DIR, 'static')

14 = https://django-bootstrap4.readthedocs.io/en/latest/installation.html

15 = views.py def index(request) :
    return render(request, 'blog/homepage.html', )

16 = css beautifier делает код красивым

17 = продумать html сайт 

=============================================================
              ---=== Занятие 10.12.2020 ===---

перемещаем папку templates в папку myproject

settings.pu -> добавить os.path.join(PROJECT_ROOT, 'templates') в TEMPLATES => DIRS
		os.path.join(PROJECT_ROOT, 'templates')
проверяем чтобы было также:
	STATIC_URL = '/static/'
	STATIC_DIR = os.path.join(PROJECT_ROOT, 'static')
	STATICFILES_DIRS = [STATIC_DIR]


шпаргалка по бутстрап4 https://bootstrap-4.ru/articles/cheatsheet/
будем делать вывод статей на сайт

полезно:
test.html -> sift + ! + enter -> шаблон html

=============================================================
              ---=== Занятие 12.12.2020 ===---
установить PostgreSQL + pgAdmin (у меня DBeaver)
создать бд (например, db_blog), задать user (например, postgres) и password (123)

подключение PostgreSQL
======= БЫЛО
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}
======= СТАЛО
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'db_blog',
        'USER' : 'postgres',
        'PASSWORD' : '123',
        'HOST' : '127.0.0.1',
        'PORT' : '5432',
        #'ENGINE': 'django.db.backends.sqlite3',
        #'NAME': BASE_DIR / 'db.sqlite3',
    }
}

// у меня так не получилось:
pip3 install psycopg2
// так получилось:
pip3 install psycopg2-binary

python3 manage.py makemigrations
python3 manage.py migrate  

Авторизация, инструкция: https://django-allauth.readthedocs.io/en/latest/installation.html
!!! при добавлении настроек следить за тем, чтобы не добавить уже существующую
Проверить: http://localhost:8001/accounts/login/

settings.py:
# редирект после авторизации
LOGIN_REDIRECT_URL = '/'
# редирект для авторизации
LOGIN_URL = '/accounts/login/'
# чтобы не получать уведомления на почту, потом можно изменить
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

для изменения внешнего вида страницы регистрации скачать account.zip
распаковать, переместить в templates

если при проверке возникнет ошибка, связанная с base.html, то:
1) либо создать файл base.html в одной из директорий выше (в зависимости от структуры проекта) и скопировать туда содержимое wrapper.html 
2) либо в новых файлах изменить путь (ниже - мой пример, у других может потребоваться изменить)
<!-- {% extends "base.html" %} -->
{% extends "../blog/wrapper.html" %} 

