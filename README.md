Простенький динамический сайт (магазин цветов) на django с авторизацией. 
Папка "additional" содержит дополнительные материалы и не является частью проекта.

Simple dynamic site (flower shop) on django with authorization.
The "additional" folder contains additional materials and is not part of the project.